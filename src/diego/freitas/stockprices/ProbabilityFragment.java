/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package diego.freitas.stockprices;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import diego.freitas.stockprices.R;

/**
 * Simple Fragment used to display some meaningful content for each page in the sample's
 * {@link android.support.v4.view.ViewPager}.
 */
public class ProbabilityFragment extends Fragment {


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	String[] probLabels = getResources().getStringArray(R.array.probability_labels);
    	Bundle arguments = getArguments();
    	int clasz = arguments.getInt("CLASS");
    	String proba = arguments.getString("PROBA");

    	View view = inflater.inflate(R.layout.probability, container, false);
    	TextView label = (TextView) view.findViewById(R.id.prob_label);
    	label.setText(probLabels[clasz]);
    	TextView prob = (TextView) view.findViewById(R.id.prob_value);
    	
    	if(proba == null || "null".equals(proba) || "".equals(proba)){
        	prob.setText("--");
    	} else {
    		double  percent = Double.valueOf(proba.split(" ")[1]) *100;
        	prob.setText(String.format("%d",(int)percent)+"%");
    	}
    	
        return view;
    }

}
