package diego.freitas.stockprices;

import android.content.Context;
import android.os.Build;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class GATracker {

	private static final String PROPERTY_ID = "UA-59776612-1";

	private Tracker tracker;

	@Inject
	public GATracker(Context ctx) {
		if(!Build.SERIAL.equals("04e08bed59ccd0ef")){
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(ctx);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			tracker = analytics.newTracker(PROPERTY_ID);
			tracker.enableAdvertisingIdCollection(true);
			tracker.enableAutoActivityTracking(true);
			tracker.enableExceptionReporting(true);
			tracker.setAnonymizeIp(false);
		}
	}

	public void openChart(String stockPrice) {
		if(tracker != null){
			tracker.send(new HitBuilders.EventBuilder("stock", "open_chart").setLabel(stockPrice).build());
		}
	}

}
