package diego.freitas.stockprices;

import roboguice.activity.RoboActionBarActivity;
import roboguice.event.Observes;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.inject.Inject;

import diego.freitas.stockprices.GPlusService.OnGoogleConnectionSuccessEvent;
import diego.freitas.stockprices.ui.slidingtabscolors.SlidingTabsColorsFragment;

public class MainActivity extends RoboActionBarActivity  implements OnClickListener{

	@Inject
	private GPlusService ggService;

	@InjectView(R.id.toolbar)
	private Toolbar toolbar;

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	
	private static final String DISCLAIMER_ACCEPTED = "disclaimer_accepted";
	private DisclaimerDialogFragment disclaimerDialogFragment;

	static final int REQUEST_CODE_PICK_ACCOUNT = 1000;

	private TextView userEmail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setSupportActionBar(toolbar);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
				R.string.app_name_title, R.string.app_name_title);
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		String[] stocks = getResources().getStringArray(R.array.menu);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);


		// Set the adapter for the list view
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, stocks));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		LayoutInflater inflater = getLayoutInflater();
		final ViewGroup header = (ViewGroup) inflater.inflate(
				R.layout.short_profile, mDrawerList, false);

		userEmail = (TextView) header.findViewById(R.id.user_email);

		mDrawerList.addHeaderView(header, null, true);
		
		 if (savedInstanceState == null) {
	            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
	            SlidingTabsColorsFragment fragment = new SlidingTabsColorsFragment();
	            transaction.replace(R.id.main_content, fragment);
	            transaction.commit();
	        }
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		mDrawerList.setItemChecked(position, true);
		mDrawerLayout.closeDrawer(mDrawerList);
		switch (position) {
		case 0:
			
			break;
		case 1:
			startActivity(new Intent(this, PurchaseActivity.class));
			break;
		case 2:
			break;
		
		}

		mDrawerList.setItemChecked(position, true);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	private void replaceContent(Fragment fragment) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.main_content, fragment)
				.commit();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(Gravity.START | Gravity.START)) {
			mDrawerLayout.closeDrawers();
			return;
		}
		super.onBackPressed();
	}

	public void loadProfileData(@Observes OnGoogleConnectionSuccessEvent event) {
		userEmail.setText(this.ggService.getUserEmail());
	}
	
	private boolean hasNotAcceptedDisclaimer() {
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		return !preferences.getBoolean(DISCLAIMER_ACCEPTED, false);
	}
	
	private void showDisclaimerDialog() {
		Fragment prev = getSupportFragmentManager().findFragmentByTag(
				DisclaimerDialogFragment.class.getName());
		if (prev == null) {
			disclaimerDialogFragment = new DisclaimerDialogFragment();
			disclaimerDialogFragment.aggrededListener = this;
			disclaimerDialogFragment.rejectListener = this;
			disclaimerDialogFragment.show(getSupportFragmentManager(),
					DisclaimerDialogFragment.class.getName());
		}
	}
	
	private void saveDisclaimerAcceptance() {
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putBoolean(DISCLAIMER_ACCEPTED, true);
		editor.commit();
		disclaimerDialogFragment.dismiss();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_agreed:
			saveDisclaimerAcceptance();
			break;
		case R.id.btn_reject:
			this.finish();
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void onResume() {
		if (hasNotAcceptedDisclaimer()) {
			showDisclaimerDialog();
		}
		super.onResume();
	}

}
