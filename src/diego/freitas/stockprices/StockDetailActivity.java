package diego.freitas.stockprices;

import com.google.inject.Inject;

import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

@ContentView(R.layout.activity_stock_detail)
public class StockDetailActivity extends RoboActionBarActivity {

	private String stockName;
	
	private StockChartFragment chartFragment;
	
	@InjectView(R.id.toolbar)
	private Toolbar toolbar;
	
	@Inject
	private  AdService adService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		stockName = getIntent().getStringExtra("stock");
		
		Bundle bundle = new Bundle();
		bundle.putString("stock", stockName);
		bundle.putString("token", getIntent().getStringExtra("token"));
		bundle.putString("email", getIntent().getStringExtra("email"));
		chartFragment = new StockChartFragment();
		chartFragment.setArguments(bundle);
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
		        .replace(R.id.fragment_container, chartFragment)
		        .commit();
		
		
		
		setSupportActionBar(toolbar);
		toolbar.setTitle(stockName);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		
	}

	public boolean onSupportNavigateUp() {
		onBackPressed();
		return true;
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		adService.showInterstitialAd();
	}
	
	

}
