package diego.freitas.stockprices.billing;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import diego.freitas.stockprices.billing.util.IabHelper;
import diego.freitas.stockprices.billing.util.IabResult;
import diego.freitas.stockprices.billing.util.Inventory;
import diego.freitas.stockprices.billing.util.Purchase;

public class BillingService {
	
	public interface BillingServiceListener {

		void onQueryInventoryFailed(IabResult result);

		void onBillingSetupFailed(IabResult result);

		void onSubscriptionVerified(boolean mSubscribedToInfiniteGas, Purchase infiniteGasPurchase);

		void onQueryInventorySuccess(IabResult result);

		void onPurchaseFailure(IabResult result);

		void onVerifyPayloadError(IabResult result);

		void onSubscriptionPurchaseSuccess(IabResult result, Purchase purchase);
		
	}
	
	static final String TAG = "TrivialDrive";

	  // Does the user have the premium upgrade?
    boolean mIsPremium = false;

    // Does the user have an active subscription to the infinite gas plan?
    boolean subscribed = false;

    // SKUs for our products: the premium upgrade (non-consumable) and gas (consumable)
    static final String SKU_PREMIUM = "premium";
    static final String SKU_GAS = "gas";

    // SKU for our subscription (infinite gas)
    static final String SKU_ALL_STOCK_PRICES = "all_stock_prices";

    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;
    
    // The helper object
    IabHelper mHelper;

	private Activity ctx;

	private BillingServiceListener listener;
    
    public BillingService(Activity ctx, BillingServiceListener listener) {
    	this.ctx = ctx;
    	this.listener = listener;
		/* base64EncodedPublicKey should be YOUR APPLICATION'S PUBLIC KEY
         * (that you got from the Google Play developer console). This is not your
         * developer public key, it's the *app-specific* public key.
         *
         * Instead of just storing the entire literal string here embedded in the
         * program,  construct the key at runtime from pieces or
         * use bit manipulation (for example, XOR with some other string) to hide
         * the actual key.  The key itself is not secret information, but we don't
         * want to make it easy for an attacker to replace the public key with one
         * of their own and then fake messages from the server.
         */
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmtZJcn9qo8biMSTbBRMox1lQM6Qoji8Nsc1mZU9Hx3OJJ2s8mv1FTAJRM5sa6QGDFRWDjvV3d7ceuuqx8iCZaVPA7gfv67K+s0o71pILilFdjrlNbFCUleQL3a13cqu3OmdLjF2KNnvA8Ykvx8pDriHIfbupJifPKnAm3Rdz4b/tW6N05RnG7BThRCOVw0yeZo+jD7nIEieWVnjghcEM/lW68F3IrIFvUes1v53d2xuQYFJJaqYWo0Qg0x+LzEa5ruURASzwzs1clJTq3r46JhJ56uKcet8inpsIkD3kecvLErO5tI5OXZTLBRtQVEt+QOxxZYuRSui5uYr+ib0exQIDAQAB";

        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(ctx, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    BillingService.this.listener.onBillingSetupFailed(result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
	}

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
            	BillingService.this.listener.onQueryInventoryFailed(result);
                
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM);
            mIsPremium = (premiumPurchase != null && verifyDeveloperPayload(premiumPurchase));
            Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));

            // Do we have the infinite gas plan?
            Purchase infiniteGasPurchase = inventory.getPurchase(SKU_ALL_STOCK_PRICES);
            subscribed = (infiniteGasPurchase != null &&
                    verifyDeveloperPayload(infiniteGasPurchase));
            Log.d(TAG, "User " + (subscribed ? "HAS" : "DOES NOT HAVE")
                        + " infinite gas subscription.");
            
            BillingService.this.listener.onSubscriptionVerified(subscribed, infiniteGasPurchase);
            BillingService.this.subsPurchase = infiniteGasPurchase;

            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
            /*Purchase gasPurchase = inventory.getPurchase(SKU_GAS);
            if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_GAS), mConsumeFinishedListener);
                return;
            }*/
            BillingService.this.listener.onQueryInventorySuccess(result);
            
            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

	public Purchase subsPurchase;
    
    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }
    

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
            	BillingService.this.listener.onPurchaseFailure(result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
            	BillingService.this.listener.onVerifyPayloadError(result);
                
                return;
            }

            Log.d(TAG, "Purchase successful.");

            /*if (purchase.getSku().equals(SKU_GAS)) {
                // bought 1/4 tank of gas. So consume it.
                Log.d(TAG, "Purchase is gas. Starting gas consumption.");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else */
            if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought the premium upgrade!
                Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
                /*alert("Thank you for upgrading to premium!");
                mIsPremium = true;
                updateUi();
                setWaitScreen(false);*/
            }
            else if (purchase.getSku().equals(SKU_ALL_STOCK_PRICES)) {
            	subscribed = true;
                // bought the infinite gas subscription
                BillingService.this.listener.onSubscriptionPurchaseSuccess(result,purchase);
                BillingService.this.subsPurchase = purchase;
            }
        }
    };

    // Called when consumption is complete
   /* IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
                mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                saveData();
                alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            }
            else {
                complain("Error while consuming: " + result);
            }
            updateUi();
            setWaitScreen(false);
            Log.d(TAG, "End consumption flow.");
        }
    };*/
    
    public boolean isSubscriberd(){
    	return subscribed;
    }

	public void dispose() {
		mHelper.dispose();
	}

	public boolean handleActivityResult(int requestCode, int resultCode,
			Intent data) {
	
		return mHelper.handleActivityResult(requestCode, resultCode, data);
	}

	public void launchSubscriptionFlow() {
		 /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = "";
        mHelper.launchPurchaseFlow(ctx,
                SKU_ALL_STOCK_PRICES, IabHelper.ITEM_TYPE_SUBS,
                RC_REQUEST, mPurchaseFinishedListener, payload);
	}

	public boolean subscriptionsSupported() {
		return mHelper.subscriptionsSupported();
	}
    

}
