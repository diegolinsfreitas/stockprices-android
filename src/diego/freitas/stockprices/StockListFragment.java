package diego.freitas.stockprices;

import java.util.List;

import com.google.inject.Inject;

import roboguice.fragment.RoboFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class StockListFragment extends RoboFragment implements OnQueryTextListener,  LoaderManager.LoaderCallbacks<List<String>>, OnClickListener {
	
	private RecyclerView mRecyclerView;
    private StockListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
	private String filter;
	
	@Inject
	private GPlusService service;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setHasOptionsMenu(true);
    }
   

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		return FontUtils.overrideFonts(getActivity(), inflater.inflate(
				R.layout.stock_list_fragment, container, false));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		AdService.loadAd(view);
		
		mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new StockListAdapter();
        mRecyclerView.setAdapter(mAdapter);
        getLoaderManager().restartLoader(0, null, this);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem item = menu.add("Search");
        item.setIcon(R.drawable.ic_action_action_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        
        //TOOD Activity is null
        SearchView sv = new SearchView(getActivity());
        sv.setOnQueryTextListener(this);
        item.setActionView(sv);

	}
	
	
	// Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ViewHolder(View v, OnClickListener listener) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.stock_name);
            mTextView.setOnClickListener(listener);
        }
    }
	
	public class StockListAdapter extends RecyclerView.Adapter<ViewHolder> {
	    private String[] mDataset = new String[0];

	    public void setData(List<String> data) {
	    	if(data == null){
	    		mDataset = new String[0];
	    	} else {
	    		mDataset = data.toArray(new String[data.size()]);
	    	}
	    	this.notifyDataSetChanged();
	    	
	    }
	    // Create new views (invoked by the layout manager)
	    @Override
	    public ViewHolder onCreateViewHolder(ViewGroup parent,
	                                                   int viewType) {
	        // create a new view
	        View v =  LayoutInflater.from(parent.getContext())
	                               .inflate(R.layout.stock_item, parent, false);
	        // set the view's size, margins, paddings and layout parameters

	        ViewHolder vh = new ViewHolder(v,StockListFragment.this);
	        return vh;
	    }

	    // Replace the contents of a view (invoked by the layout manager)
	    @Override
	    public void onBindViewHolder(ViewHolder holder, int position) {
	        // - get element from your dataset at this position
	        // - replace the contents of the view with that element
	        holder.mTextView.setText(mDataset[position]);

	    }

	    // Return the size of your dataset (invoked by the layout manager)
	    @Override
	    public int getItemCount() {
	        return mDataset.length;
	    }
	}

	@Override
	public boolean onQueryTextChange(String text) {
		filter = !TextUtils.isEmpty(text) ? text : null;
        getLoaderManager().restartLoader(0, null, this);
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Loader<List<String>> onCreateLoader(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		return new StocksListLoader(getActivity(), filter);
	}

	@Override
	public void onLoadFinished(Loader<List<String>> arg0, List<String> data) {
		 // Set the new data in the adapter.
        mAdapter.setData(data);

        // The list should now be shown.
        /*if (isResumed()) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }*/
	}

	@Override
	public void onLoaderReset(Loader<List<String>> data) {
		 // Clear the data in the adapter.
        mAdapter.setData(null);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(getActivity(), StockDetailActivity.class);
		intent.putExtra("stock", ((TextView)v).getText());
		intent.putExtra("token", service.authToken);
		intent.putExtra("email", service.getUserEmail());
		startActivity(intent);
	}
	
	
}
