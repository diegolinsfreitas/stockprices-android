package diego.freitas.stockprices;

import roboguice.RoboGuice;
import android.app.Application;


public class StockPricesApplication extends Application{
	
	@Override
	public void onCreate() {
		super.onCreate();
		RoboGuice.setUseAnnotationDatabases(false);
	}

}
