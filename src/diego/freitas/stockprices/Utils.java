package diego.freitas.stockprices;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;

public class Utils {

	public static boolean isDebug(Context ctx) {
		String packageName = ctx.getPackageName();
		int flags;
		try {
			flags = ctx.getPackageManager().getApplicationInfo(packageName, 0).flags;
			return (flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
		} catch (NameNotFoundException e) {
			return false;
		}
	}

}
