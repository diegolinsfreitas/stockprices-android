package diego.freitas.stockprices;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import roboguice.event.EventManager;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.XLabels.XLabelPosition;
import com.github.mikephil.charting.utils.YLabels;
import com.github.mikephil.charting.utils.YLabels.YLabelPosition;
import com.google.inject.Inject;
import com.viewpagerindicator.UnderlinePageIndicator;

public class StockChartFragment extends RoboFragment implements OnChartValueSelectedListener {

	private boolean chartReady;
	private Typeface DEFAULT_TYPEFACE;
	private SimpleDateFormat simpleDateFormat;
	private SimpleDateFormat xlFormatter;
	
	@Inject
	private GATracker gaTracker;

	@InjectView(R.id.chart)
	private LineChart chart;
	
	@InjectView(R.id.close_price_real)
	private TextView closePrice;
	
	@InjectView(R.id.close_price_pred)
	private TextView closePricePred;
	

	@Inject
	EventManager evtManager;

	@InjectView(R.id.load_progress)
	private ProgressBar loadProgress;
	private String stockCode;
	private ValueFormatter yValueFormatter = new ValueFormatter() {

		@Override
		public String getFormattedValue(float value) {
			return String.format("%.2f", value);
		}
	};
	private String token;
	private String email;
	private AsyncTask<String, Void, JSONObject> loadTask;
	private LineDataSet closePriceDataSet;
	private LineDataSet pastPredictionsDataSet;
	private LineDataSet futurePredictionsDataSet;
	private ViewPager mViewPager;
	private JSONArray probabilities;
	private ProbabilitiesFragmentPagerAdapter pagerAdapter;
	private int pageSize =10;
	private Menu menu;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		setHasOptionsMenu(true);
		return FontUtils.overrideFonts(getActivity(), inflater.inflate(
				R.layout.stock_chart_fragment, container, false));

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		AdService.loadAd(view);
		
		Bundle args = getArguments();
		DEFAULT_TYPEFACE = FontUtils.getTypeFaceByName(getActivity(),
				FontUtils.ROBOTO_LIGHT);

		stockCode = args.getString("stock", "");
		token = args.getString("token", "");
		email = args.getString("email", "");
		loadProgress.setVisibility(View.VISIBLE);
		
		mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        pagerAdapter = new ProbabilitiesFragmentPagerAdapter(getChildFragmentManager());
		mViewPager.setAdapter(pagerAdapter);
        mViewPager.setCurrentItem(0);
        UnderlinePageIndicator underLineIndicator = (UnderlinePageIndicator)view.findViewById(R.id.titles);
        underLineIndicator.setFades(false);
        underLineIndicator.setViewPager(mViewPager);
		setupChart();
		loadStockData(stockCode);

	}

	private void loadStockData(String stockName) {

		gaTracker.openChart(stockName);

		loadTask = new AsyncTask<String, Void, JSONObject>() {

			protected void onPreExecute() {
				loadProgress.setVisibility(View.VISIBLE);
				chart.setVisibility(View.INVISIBLE);
				mViewPager.setEnabled(false);
				probabilities = null;
			};

			@Override
			protected JSONObject doInBackground(String... params) {
				StringBuilder builder = new StringBuilder("");
				InputStream in = null;
				int responseCode = -1;
				try {
					URL url = new URL(
							String.format(
									//"https://stockprices-diegofreitas.rhcloud.com/app/api/stockprices/?format=json&stock_name=%s&page=%d",
									"http://192.168.0.15:8000/app/api/stockprices/?format=json&stock_name=%s&page=%d",
									params[0], pageSize));
					HttpURLConnection urlConnection = (HttpURLConnection) url
							.openConnection();
					urlConnection
							.addRequestProperty("Authorization", params[1]);
					urlConnection.addRequestProperty("email", params[2]);
					responseCode = urlConnection.getResponseCode();
					in = new BufferedInputStream(urlConnection.getInputStream());
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}

				} catch (IOException e) {

					if (responseCode == 503 || responseCode == 500) {
						showToast("Serviço indisponível no momento");
					} else {
						showToast("Verifique sua conexão com a internet");
					}

					return new JSONObject();
				} finally {
					try {
						if (in != null) {
							in.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				try {
					return new JSONObject(builder.toString());
				} catch (JSONException e) {
					return new JSONObject();
				}
			}

			protected void onPostExecute(JSONObject result) {
				chart.setVisibility(View.VISIBLE);
				loadProgress.setVisibility(View.INVISIBLE);
				updateChart(result);
				mViewPager.setEnabled(true);
			};
		};

		loadTask.execute(stockCode, token, email);

	}

	protected void showToast(final String message) {
		if(getActivity() != null){
			getActivity().runOnUiThread(new Runnable() {
	
				@Override
				public void run() {
					Toast.makeText(getActivity(), message, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
	}

	private void setupChart() {
		if (chartReady) {
			return;
		}
		// chart.setUnit("R$ ");
		
		chart.setDescription("");
		chart.setDrawBorder(false);
		chart.setHighlightEnabled(true);
		
		chart.setDrawVerticalGrid(false);
		chart.setDrawHorizontalGrid(false);
		chart.setDrawYValues(false);

		chart.setValueFormatter(yValueFormatter);
		

		chart.setValueTypeface(DEFAULT_TYPEFACE);
		chart.setValueTextSize(16f);
		chart.setDrawXLabels(true);
		XLabels xl = chart.getXLabels();
		xl.setPosition(XLabelPosition.BOTTOM); // set the position
		xl.setTypeface(DEFAULT_TYPEFACE); // set a different font
		xl.setTextSize(18f); // set the textsize
		xl.setTextColor(getResources().getColor(R.color.primary_dark));

		YLabels yl = chart.getYLabels();
		yl.setPosition(YLabelPosition.RIGHT); // set the position
		yl.setTypeface(DEFAULT_TYPEFACE); // set a different font
		yl.setTextSize(16f); // set the textsize
		yl.setTextColor(getResources().getColor(R.color.primary_dark));
		yl.setFormatter(new ValueFormatter() {

			@Override
			public String getFormattedValue(float value) {
				// TODO Auto-generated method stub
				return String.format("%.1f", value);
			}
		});
		chart.setDrawLegend(false);
		
		chartReady = true;
		chart.setOnChartValueSelectedListener(this);
	}

	private void updateChart(JSONObject result) {

		if (!result.has("objects")) {
			chart.clear();
		}

		try {

			ArrayList<Entry> realValue = new ArrayList<Entry>();
			ArrayList<Entry> futurePredictedValue = new ArrayList<Entry>();
			ArrayList<Entry> pastPredictedValue = new ArrayList<Entry>();
			ArrayList<String> xVals = new ArrayList<String>();
			JSONArray stockPrices = result.getJSONArray("objects");

			int last_index = stockPrices.length() - 1;
			for (int i = 0; i < stockPrices.length(); i++) {
				JSONObject stockPrice = stockPrices.getJSONObject(i);

				Entry c1e1 = new Entry(
						(float) stockPrice.getDouble("close_price"), i);
				realValue.add(c1e1);
				
				c1e1.setData(stockPrice.getJSONArray("probabilities"));

				float predicted = 0f;
				if (!stockPrice.isNull("predicted_price")) {
					predicted = (float) stockPrice.getDouble("predicted_price");
					Entry pastEntry = new Entry(predicted, i);
					pastEntry.setData(stockPrice.getJSONArray("probabilities"));
					pastPredictedValue.add(pastEntry);
				}

				xVals.add(getXLabel(stockPrice.getString("date")));

				if (last_index == i && !stockPrice.isNull("future_predictions")) {
					JSONArray futurePreds = stockPrice
							.getJSONArray("future_predictions");
					Entry tempEntry = null;
					// entry added in order to join the past and future
					// predictions lines
					if (!stockPrice.isNull("predicted_price")) {
						tempEntry = new Entry(
								(float) stockPrice.getDouble("predicted_price"),
								i);
						futurePredictedValue.add(tempEntry);
					}
					JSONObject prediction = futurePreds.getJSONObject(0);

					if (prediction.isNull("R1")) {
						break;
					}
					tempEntry = new Entry((float) prediction.getDouble("R1"),
							i + 1);
					futurePredictedValue.add(tempEntry);
					xVals.add("1 d");

					prediction = futurePreds.getJSONObject(1);
					tempEntry = new Entry((float) prediction.getDouble("R2"),
							i + 2);
					futurePredictedValue.add(tempEntry);
					xVals.add("2 d");

					prediction = futurePreds.getJSONObject(2);
					tempEntry = new Entry((float) prediction.getDouble("R3"),
							i + 3);
					futurePredictedValue.add(tempEntry);
					xVals.add("3 d");

					prediction = futurePreds.getJSONObject(3);
					tempEntry = new Entry((float) prediction.getDouble("R4"),
							i + 4);
					futurePredictedValue.add(tempEntry);
					xVals.add("5 d");

				}

			}

			closePriceDataSet = new LineDataSet(realValue, "Preço");


			pastPredictionsDataSet = new LineDataSet(
					pastPredictedValue, "Previsto");


			futurePredictionsDataSet = new LineDataSet(
					futurePredictedValue, "Previsão");

			futurePredictionsDataSet.enableDashedLine(10f, 10f, 0f);

			styleLine(pastPredictionsDataSet, getResources().getColor(R.color.pred_close_price));
			styleLine(futurePredictionsDataSet, getResources().getColor(R.color.pred_close_price));
			styleLine(closePriceDataSet, getResources().getColor(R.color.close_price));
			

			ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
			dataSets.add(closePriceDataSet);
			dataSets.add(pastPredictionsDataSet);
			dataSets.add(futurePredictionsDataSet);

			LineData data = new LineData(xVals, dataSets);
			
			

			
			chart.setYRange(data.getYMin() * 0.8f, data.getYMax() * 1.20f, false);
			chart.setData(data);
			chart.setBackgroundColor(getResources().getColor(android.R.color.white));

			chart.animateY(1000);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void styleLine(LineDataSet dataSet, int color) {
		dataSet.setLineWidth(3f);
		dataSet.setCircleSize(5f);
		dataSet.setCircleColor(color);
		dataSet.setColor(color);
		dataSet.setDrawFilled(true);
		dataSet.setFillAlpha(125);
		dataSet.setFillColor(color);
	}

	private String getXLabel(String dateValue) {
		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss",
				new Locale("pt", "BR"));
		xlFormatter = new SimpleDateFormat("dd/MMM", new Locale("pt", "BR"));
		try {
			Date parsedDate = simpleDateFormat.parse(dateValue);
			return xlFormatter.format(parsedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateValue.substring(5, 10);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		this.menu = menu;
		
        
        MenuItem page10 = menu.add(2, R.id.ic_action_10days, 0, "10d\u2713");
        page10.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        page10.setCheckable(true);
        
        
        MenuItem page20 = menu.add(1, R.id.ic_action_20days, 0, "20d");
        page20.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        page20.setCheckable(true);
        
        /*MenuItem page30 = menu.add(0, R.id.ic_action_30days, 0, "30d");
        page30.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        page30.setCheckable(true);*/
        
        MenuItem item = menu.add(3, R.id.ic_action_refresh, 0, "Refresh");
        item.setIcon(R.drawable.ic_action_refresh);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		super.onCreateOptionsMenu(menu, inflater);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		MenuItem page10 = this.menu.findItem(R.id.ic_action_10days);
		MenuItem page20 = this.menu.findItem(R.id.ic_action_20days);
		//MenuItem page30 = this.menu.findItem(R.id.ic_action_30days);
		
		switch (item.getItemId()) {
		case R.id.ic_action_refresh:
			loadStockData(stockCode);
			return true;
		case R.id.ic_action_10days:
			pageSize = 10;
			page10.setTitle("10D\u2713");
			page20.setTitle("20D");
			//page30.setTitle("30D");
			loadStockData(stockCode);
			return true;
		case R.id.ic_action_20days:
			pageSize = 20;
			page20.setTitle("20D\u2713");
			page10.setTitle("10D");
			//page30.setTitle("30D");
			loadStockData(stockCode);
			return true;
		case R.id.ic_action_30days:
			pageSize = 30;
			//page30.setTitle("30D\u2713");
			page20.setTitle("20D");
			page10.setTitle("10D");
			loadStockData(stockCode);
			return true;
		default:
			return false;
		}
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		if (loadTask != null && loadTask.getStatus() != Status.FINISHED){
			loadTask.cancel(true);
		}
	}

	@Override
	public void onValueSelected(Entry e, int dataSetIndex) {
		closePrice.setText("--");
		closePricePred.setText("--");
		if(dataSetIndex == 2) {
			closePricePred.setText(yValueFormatter.getFormattedValue(e.getVal()));
		} else if(dataSetIndex == 1){
			closePricePred.setText(yValueFormatter.getFormattedValue(e.getVal()));
			Entry closePriceEntry = this.closePriceDataSet.getEntryForXIndex(e.getXIndex());
			closePrice.setText(yValueFormatter.getFormattedValue(closePriceEntry.getVal()));
		} else {
			closePrice.setText(yValueFormatter.getFormattedValue(e.getVal()));
			Entry closePricePredEntry = this.pastPredictionsDataSet.getEntryForXIndex(e.getXIndex());
			if(closePricePredEntry != null){
				closePricePred.setText(yValueFormatter.getFormattedValue(closePricePredEntry.getVal()));
			} else {
				closePricePred.setText("--");
			}
		}
		
		if(e.getData() != null){
			probabilities = (JSONArray) e.getData();
		} else {
			probabilities = null;
		}
		
		mViewPager.setCurrentItem(0);
		pagerAdapter.notifyDataSetChanged();
	}

	@Override
	public void onNothingSelected() {
		closePrice.setText("--");
		closePricePred.setText("--");
		probabilities = null;
		mViewPager.setCurrentItem(0);
		pagerAdapter.notifyDataSetChanged();
	}
	
	class ProbabilitiesFragmentPagerAdapter extends FragmentStatePagerAdapter {


		ProbabilitiesFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            
        }

        /**
         * Return the {@link android.support.v4.app.Fragment} to be displayed at {@code position}.
         * <p>
         * Here we return the value returned from {@link SamplePagerItem#createFragment()}.
         */
        @Override
        public Fragment getItem(int position) {
        	Fragment fragment = new ProbabilityFragment();
        	Bundle bundle = new Bundle();
        	try {
        		String proba = "";
        		if(probabilities != null){
        			proba = probabilities.getJSONObject(position).getString("N"+(position+1));
        		}
				bundle.putInt("CLASS", position);
				bundle.putString("PROBA", proba);
				fragment.setArguments(bundle);
        	} catch (JSONException e) {
				e.printStackTrace();
			}
        	return fragment;
        }
        
        public int getItemPosition(Object object){
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

}
