package diego.freitas.stockprices;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;

public class DisclaimerDialogFragment extends DialogFragment {

	OnClickListener rejectListener;
	OnClickListener aggrededListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setCancelable(false);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.setCanceledOnTouchOutside(false);
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.disclaimer, container, false);
		FontUtils.overrideFonts(getActivity(), FontUtils.ROBOTO_LIGHT, v);
		v.findViewById(R.id.btn_reject).setOnClickListener(rejectListener);
		v.findViewById(R.id.btn_agreed).setOnClickListener(
				aggrededListener);
		return v;
	}

}
