package diego.freitas.stockprices;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.inject.Inject;

public class AdService {

	private static final int TOTAL_INTERACTION_SHOW_FULLAD = 5;

	private static final String TEST_DEVICE = "78CB2C84D47C251A0949AA4F74B0B2F9";

	private SharedPreferences prefs;

	private Activity ctx;

	@Inject
	public AdService(Activity ctx) {
		this.ctx = ctx;
		interstitialAd = new InterstitialAd(ctx);
		interstitialAd.setAdUnitId(AD_UNIT_ID_INTERSTITIAL);
		prefs = ctx.getSharedPreferences("ads", Context.MODE_PRIVATE);
	}

	public static void loadAd(Activity activity) {
		AdView adView = (AdView) activity.findViewById(R.id.adView);
		doRequestAd(adView);
	}

	public static void loadAd(View view) {
		AdView adView = (AdView) view.findViewById(R.id.adView);
		doRequestAd(adView);
	}

	private static void doRequestAd(AdView adView) {
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(TEST_DEVICE).build();
		adView.loadAd(adRequest);
	}

	private InterstitialAd interstitialAd;

	private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-0555397782109166/3161094795";

	public void loadInterstitialAd() {
		AdRequest.Builder builder = new AdRequest.Builder();
		builder.addTestDevice(TEST_DEVICE);
		AdRequest interstitialRequest = builder.build();
		interstitialAd.loadAd(interstitialRequest);
	}

	public void showInterstitialAd() {
		int openInterAdTrigger = prefs.getInt("count", 0);
		if (openInterAdTrigger == TOTAL_INTERACTION_SHOW_FULLAD) {
			showOrLoadInterstital();
			openInterAdTrigger = 0;
		}

		openInterAdTrigger++;
		prefs.edit().putInt("count", openInterAdTrigger).commit();
	}

	private void showOrLoadInterstital() {
		try {
			ctx.runOnUiThread(new Runnable() {

				public void run() {

					interstitialAd.setAdListener(new AdListener() {
						@Override
						public void onAdLoaded() {
							interstitialAd.show();
						}

						@Override
						public void onAdClosed() {

						}
					});

					loadInterstitialAd();
				}
			});

		} catch (Exception e) {
			Log.e(MainActivity.class.getName(), e.getMessage());
		}

	}

}
