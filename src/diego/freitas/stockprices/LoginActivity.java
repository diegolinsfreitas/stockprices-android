package diego.freitas.stockprices;

import roboguice.activity.RoboActionBarActivity;
import roboguice.event.Observes;
import roboguice.inject.ContentView;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import diego.freitas.stockprices.GPlusService.OnGPlusLoginSuccesssEvent;

@ContentView(R.layout.activity_login)
public class LoginActivity extends RoboActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(Utils.isDebug(this)){
			onLoginSuccess(null);
		} else {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frag_content, new GPlusLoginFragment()).commit();
		}
	}

	public void onLoginSuccess(@Observes OnGPlusLoginSuccesssEvent event) {
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}

}
