package diego.freitas.stockprices;

import roboguice.fragment.RoboFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class TradeCalculatorFragment extends RoboFragment implements  OnClickListener {
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setHasOptionsMenu(true);
    }
   

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		return FontUtils.overrideFonts(getActivity(), inflater.inflate(
				R.layout.stock_list_fragment, container, false));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		AdService.loadAd(view);
		
		
	}

	@Override
	public void onClick(View v) {
		
	}
	
	
}
