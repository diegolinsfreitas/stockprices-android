package diego.freitas.stockprices;

import roboguice.event.Observes;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.google.inject.Inject;

import diego.freitas.stockprices.GPlusService.OnGoogleConnectionFailedEvent;

public class GPlusLoginFragment extends RoboFragment implements OnClickListener {

	@InjectView(R.id.sign_in_button)
	private View btnSignGplus;

	@InjectView(R.id.login_panel)
	private View loginPanel;

	@InjectView(R.id.load_progress)
	private View pbAuth;

	@Inject
	private GPlusService service;

	private boolean signinClicked;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		return FontUtils.overrideFonts(getActivity(), inflater.inflate(
				R.layout.gplus_login_fragment, container, false));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		btnSignGplus.setOnClickListener(this);
	}

	public void onConnectionFailed(@Observes OnGoogleConnectionFailedEvent event) {
		if (this.service.hasResolutionForError() && signinClicked
				&& !service.cancelled) {
			this.service.resolveSignInError();
		} else {
			loginPanel.setVisibility(View.VISIBLE);
			pbAuth.setVisibility(View.INVISIBLE);
			// fragView.setBackgroundColor(getResources().getColor(R.color.primary));
			if (signinClicked && !service.cancelled) {
				new AlertDialog.Builder(getActivity())
						.setMessage("Erro ao connectar. Tente novamente")
						.create().show();
			}
			signinClicked = false;
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.sign_in_button && !service.isConnecting()) {
			signinClicked = true;
			service.cancelled = false;
			loginPanel.setVisibility(View.INVISIBLE);
			pbAuth.setVisibility(View.VISIBLE);
			service.resolveSignInError();

		}
	}

}
