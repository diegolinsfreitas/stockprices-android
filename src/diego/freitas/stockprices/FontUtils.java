package diego.freitas.stockprices;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FontUtils {
	
	public static final String ROBOTO_LIGHT = "Roboto-Light";
	
	private static Map<String,Typeface> typefaces = new HashMap<String,Typeface>();
	
	public static View overrideFonts(final Context context,final View v) {
		return overrideFonts(context, ROBOTO_LIGHT, v);
	}

	public static View overrideFonts(final Context context, String fontName, final View v) {
		try {
			if (v instanceof ViewGroup) {
				ViewGroup vg = (ViewGroup) v;
				for (int i = 0; i < vg.getChildCount(); i++) {
					View child = vg.getChildAt(i);
					overrideFonts(context, fontName, child);
				}
			} else {
				Typeface typeface= getTypeFaceByName(context, fontName);
				if (v instanceof TextView) {
					((TextView) v).setTypeface(typeface);
				} else if (v instanceof Button) {
					((Button) v).setTypeface(typeface);
				}
			}
		} catch (Exception e) {
		}
		return v;
	}

	public static Typeface getTypeFaceByName(final Context context, String fontName) {
		if(!typefaces.containsKey(fontName)){
			typefaces.put(fontName, Typeface.createFromAsset(
					context.getAssets(), fontName+".ttf"));
		}
		return typefaces.get(fontName);
	}
}
