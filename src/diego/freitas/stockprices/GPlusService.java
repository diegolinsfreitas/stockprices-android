package diego.freitas.stockprices;

import java.io.IOException;
import java.io.InputStream;

import roboguice.activity.event.OnActivityResultEvent;
import roboguice.activity.event.OnStopEvent;
import roboguice.context.event.OnStartEvent;
import roboguice.event.EventManager;
import roboguice.event.Observes;
import roboguice.inject.ContextSingleton;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender.SendIntentException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.inject.Inject;


@ContextSingleton
public class GPlusService implements ConnectionCallbacks,
		OnConnectionFailedListener {

	public static class OnGPlusLoginSuccesssEvent {

	}
	
	public static class OnGPlusLoginFailedEvent {

		public String code;

		public OnGPlusLoginFailedEvent(String code) {
			this.code = code;
		}

	}

	public static class OnGoogleConnectionSuccessEvent {
		
	}

	private GoogleApiClient client;
	private Activity activity;
	// Profile pic image size in pixels
    private static final int PROFILE_PIC_SIZE = 50;

	@Inject
	private EventManager evtManager;

	/*
	 * Store the connection result from onConnectionFailed callbacks so that we
	 * can resolve them when the user clicks sign-in.
	 */
	private ConnectionResult mConnectionResult;

	/* Request code used to invoke sign in user interactions. */
	private static final int RC_SIGN_IN = 0;

	private boolean mIntentInProgress;
	public String authToken;
	private GetAuthTockenTask getUsernameTask;
	private String personGooglePlusProfile;
	private String personPhotoUrl;
	private String personName;
	private String email;
	public boolean cancelled;

	@Inject
	public GPlusService(Context ctx, Activity currentAct) {
		activity = currentAct;
		client = new GoogleApiClient.Builder(ctx).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API)
				.addScope(Plus.SCOPE_PLUS_LOGIN).addScope(Plus.SCOPE_PLUS_PROFILE).build();
		Log.i(GPlusService.class.getName(), "GPlusService Created");
	}

	public void startService(@Observes OnStartEvent<Context> event) {
		client.connect();
	}

	public void stopService(@Observes OnStopEvent event) {
		if (client.isConnected()) {
			client.disconnect();
		}
	}

	public boolean isConnected() {
		return client.isConnected();
	}

	public void onActivityResult(@Observes OnActivityResultEvent event) {
		if (event.getRequestCode() == RC_SIGN_IN && event.getResultCode() != 0) {
			mIntentInProgress = false;
			if (!client.isConnecting()) {
				client.connect();
			}
		} else {
			cancelled = true;
			evtManager.fire(new OnGoogleConnectionFailedEvent());
		}
	}

	/* A helper method to resolve the current ConnectionResult error. */
	public void resolveSignInError() {
		if (hasResolutionForError() && !cancelled) {
			try {
				mIntentInProgress = true;
				activity.startIntentSenderForResult(mConnectionResult
						.getResolution().getIntentSender(), RC_SIGN_IN, null,
						0, 0, 0);
			} catch (SendIntentException e) {
				// The intent was canceled before it was sent. Return to the
				// default
				// state and attempt to connect to get an updated
				// ConnectionResult.
				mIntentInProgress = false;
				client.connect();
			}
		}
	}

	public boolean hasResolutionForError() {
		return mConnectionResult != null && mConnectionResult.hasResolution();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), activity,
					0).show();
			return;
		}
		if (!mIntentInProgress) {
			// Store the ConnectionResult so that we can use it later when the
			// user clicks
			// 'sign-in'.
			mConnectionResult = result;
			evtManager.fire(new OnGoogleConnectionFailedEvent());

		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		//getProfileInformation();
		
		getUsernameTask = new GetAuthTockenTask();
		getUsernameTask.emailAccount = email = Plus.AccountApi.getAccountName(client);
		getUsernameTask.execute();
		
		evtManager.fire(new OnGoogleConnectionSuccessEvent());
	}
	
	/**
	 * Fetching user's information name, email, profile pic
	 * */
	private void getProfileInformation() {
	    try {
	        if (Plus.PeopleApi.getCurrentPerson(client) != null) {
	            Person currentPerson = Plus.PeopleApi
	                    .getCurrentPerson(client);
	            personName = currentPerson.getDisplayName();
	            personPhotoUrl = currentPerson.getImage().getUrl();
	            personGooglePlusProfile = currentPerson.getUrl();
	            email = Plus.AccountApi.getAccountName(client);
	 
	            // by default the profile url gives 50x50 px image only
	            // we can replace the value with whatever dimension we want by
	            // replacing sz=X
	            personPhotoUrl = personPhotoUrl.substring(0,
	                    personPhotoUrl.length() - 2)
	                    + PROFILE_PIC_SIZE;
	 
	            
	 
	        } else {
	            Toast.makeText(activity.getApplicationContext(),
	                    "Person information is null", Toast.LENGTH_LONG).show();
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	 
	/**
	 * Background Async task to load user profile picture from url
	 * */
	public static class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;
	 
	    public LoadProfileImage(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }
	 
	    protected Bitmap doInBackground(String... urls) {
	        String urldisplay = urls[0];
	        Bitmap mIcon11 = null;
	        try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }
	        return mIcon11;
	    }
	 
	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}

	public String getUserName() {
		return personName;
	}
	
	public String getUserEmail() {
		return email;
	}

	@Override
	public void onConnectionSuspended(int cause) {

	}

	/**
	 * Revoke Google+ authorization completely.
	 */
	public void revokeAccess() {
		if (client.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(client);
			Plus.AccountApi.revokeAccessAndDisconnect(client);

		}

	}

	/**
	 * Sign out the user (so they can switch to another account).
	 */
	public void signOut() {
		if (client.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(client);
		}
	}

	public static class OnGoogleConnectionFailedEvent {

	}

	private class GetAuthTockenTask extends AsyncTask<Void, Void, String> {

		protected String emailAccount;

		@Override
		protected String doInBackground(Void... arg0) {
			// Bundle appActivities = new Bundle();
			// appActivities.putString(GoogleAuthUtil.KEY_REQUEST_VISIBLE_ACTIVITIES);
			try {
				return authToken = GoogleAuthUtil
						.getToken(
								activity,
								emailAccount,
								"audience:server:client_id:480244832353-8lprvogrr6jgce3kis7geng1v4tbjigm.apps.googleusercontent.com");
			} catch (UserRecoverableAuthException userRecoverableException) {
				// GooglePlayServices.apk is either old, disabled, or not
				// present
				// so we need to show the user some UI in the activity to
				// recover.
				evtManager.fire(new OnGPlusLoginFailedEvent("Atualize o Google Play Services"));
				
			} catch (GoogleAuthException fatalException) {
				evtManager.fire(new OnGPlusLoginFailedEvent("Erro ao autenticar o usuário"));
			} catch (IOException e) {
				evtManager.fire(new OnGPlusLoginFailedEvent("Verifique a sua conexão"));
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				evtManager.fire(new OnGPlusLoginSuccesssEvent());
			}
		}

	}

	public boolean isConnecting() {
		return client.isConnecting();
	}
	
	public String getPersonPhotoUrl() {
		return personPhotoUrl;
	}
}
